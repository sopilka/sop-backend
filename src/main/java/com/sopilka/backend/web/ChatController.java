package com.sopilka.backend.web;


import com.sopilka.backend.domain.AiService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("ai")
public class ChatController {
    private final AiService aiService;

    public ChatController(AiService aiService) {
        this.aiService = aiService;
    }

    @PostMapping("/author")
    public ResponseEntity<String> getAuthorInfo(@RequestBody AuthorInfoRequestDto authorsDto) {
        return ResponseEntity.ok(aiService.getInfoAboutAuthor(authorsDto.author()));
    }

    @PostMapping("/ask")
    public ResponseEntity<String> ask(
            @RequestHeader(name = "AI_CONVERSATION_ID", defaultValue = "default") String conversationId,
            @RequestBody QuestionDto questionDto) {
        return ResponseEntity.ok(aiService.ask(questionDto, conversationId));
    }
}
