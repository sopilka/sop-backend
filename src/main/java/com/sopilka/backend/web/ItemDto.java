package com.sopilka.backend.web;

import com.sopilka.backend.domain.ItemType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItemDto {
    private String title;
    private int price;
    private List<String> authors;
    private String label;
    private int releaseYear;
    private ItemType itemType;
    private List<String> imageUrls;
    private Set<String> genres;
}
