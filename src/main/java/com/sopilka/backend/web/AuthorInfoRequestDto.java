package com.sopilka.backend.web;

public record AuthorInfoRequestDto(String author) {}
