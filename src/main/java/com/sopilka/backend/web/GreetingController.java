package com.sopilka.backend.web;

import com.sopilka.backend.config.SopilkaProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("greeting")
public class GreetingController {

    private final SopilkaProperties sopilkaProperties;

    public GreetingController(SopilkaProperties sopilkaProperties) {
        this.sopilkaProperties = sopilkaProperties;
    }

    @GetMapping
    ResponseEntity<String> greeting() {
        return ResponseEntity.ok(sopilkaProperties.getGreeting());
    }
}
