package com.sopilka.backend.web;

import com.sopilka.backend.domain.genre.GenreService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("genres")
public class GenreController {

    final GenreService genreService;

    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping
    ResponseEntity<List<String>> findGenres(@RequestParam("query") String query) {
        return ResponseEntity.ok(genreService.find(query));
    }

    @PostMapping
    ResponseEntity<String> create(@RequestParam String name) {
        genreService.create(name);
        return ResponseEntity.ok(name);
    }
}
