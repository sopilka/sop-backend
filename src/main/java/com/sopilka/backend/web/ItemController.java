package com.sopilka.backend.web;

import com.sopilka.backend.domain.Item;
import com.sopilka.backend.domain.ItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("items")
public class ItemController {

    private final ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("{id}")
    ResponseEntity<Item> get(@PathVariable("id") String uuid) {
        return ResponseEntity.ok(itemService.findById(UUID.fromString(uuid)));
    }

    @GetMapping
    ResponseEntity<List<ItemDto>> getAll() {
        return ResponseEntity.ok(itemService.findAll());
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<Item> add(@ModelAttribute AddItem itemToAdd) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(itemService.add(itemToAdd));
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable("id") UUID id) {
        itemService.delete(id);
    }

    @PutMapping("{id}/genres")
    ResponseEntity<String> addGenre(@PathVariable("id") UUID id, @RequestParam String genre) {
        itemService.addGenre(id, genre);
        return ResponseEntity.ok("Genre was added");
    }
}
