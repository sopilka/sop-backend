package com.sopilka.backend.web;

import com.sopilka.backend.domain.ItemType;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public record AddItem(
        @NotBlank(message = "The item title should be defined.")
        String title,
        Integer price,
        Integer quantity,
        String label,
        Integer releaseYear,
        String itemType,
        MultipartFile[] images,
        UUID[] authorIds) {
}
