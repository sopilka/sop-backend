package com.sopilka.backend.domain;

public enum ItemType {
    VINYL, CASSETTE, CD
}
