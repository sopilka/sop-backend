package com.sopilka.backend.domain;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "image_urls")
@NoArgsConstructor
public class ImageUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    private Item item;

    private String url;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public ImageUrl(Item item, String url) {
        this.item = item;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
