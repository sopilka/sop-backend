package com.sopilka.backend.domain;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "sopilka.testdata.enabled", havingValue = "true")
public class ItemsDataLoader {
    private final ItemRepository itemRepository;

    public ItemsDataLoader(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void loadItemsTestData() {
        itemRepository.save(new Item(
                "Francis Albert Sinatra & Antonio Carlos Jobim",
                4,
                1,
                "Reprise Records",
                1967, ItemType.VINYL));

        itemRepository.save(new Item(
                "Естрадний Ансамбль Арніка",
                10,
                1,
                "Мелодия",
                1974, ItemType.VINYL));
        itemRepository.save(new Item(
                "Somniloquy",
                15,
                1,
                "Bloomed In September Tapes",
                2024, ItemType.VINYL));
    }
}
