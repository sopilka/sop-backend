package com.sopilka.backend.domain.genre;

import jakarta.persistence.EntityExistsException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {

    final GenreRepository genreRepository;

    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public List<String> findAll() {
        return genreRepository.findAll().stream().map(Genre::getName).toList();
    }

    public List<String> find(String query) {
        return genreRepository.findByNameContainingIgnoreCase(query);
    }

    public boolean exists(String name) {
        return genreRepository.findById(name).isPresent();
    }

    public String create(String name) {
        if (exists(name)) {
            throw new EntityExistsException();
        }
        return genreRepository.save(new Genre(name)).getName();
    }

}
