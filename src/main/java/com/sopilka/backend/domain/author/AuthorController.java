package com.sopilka.backend.domain.author;

import com.sopilka.backend.web.AuthorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("authors")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    ResponseEntity<List<AuthorNameDto>> findAuthors(@RequestParam("query") String query) {
        return ResponseEntity.ok(authorService.find(query));
    }

    @PostMapping
    ResponseEntity<AuthorDto> create(@RequestParam("name") String name) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(authorService.create(name));
    }
}
