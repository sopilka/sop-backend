package com.sopilka.backend.domain.author;

import com.sopilka.backend.web.AuthorDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public List<AuthorNameDto> find(String query) {
        return authorRepository.findByNameContainingIgnoreCase(query);
    }

    @Transactional
    public AuthorDto create(String name) {
        Author saved = authorRepository.save(new Author(name));
        return AuthorDto.builder()
                .id(saved.getId())
                .name(saved.getName())
                .createdDate(saved.getCreatedDate()).build();
    }
}
