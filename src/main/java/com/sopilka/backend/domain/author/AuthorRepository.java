package com.sopilka.backend.domain.author;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AuthorRepository extends JpaRepository<Author, UUID> {
    List<AuthorNameDto> findByNameContainingIgnoreCase(String name);
}
