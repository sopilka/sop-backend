package com.sopilka.backend.domain.author;

import java.util.UUID;

public interface AuthorNameDto {
    String getName();
    UUID getId();
}
