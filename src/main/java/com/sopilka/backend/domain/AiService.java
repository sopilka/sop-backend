package com.sopilka.backend.domain;

import com.sopilka.backend.web.AuthorsDto;
import com.sopilka.backend.web.QuestionDto;
import lombok.SneakyThrows;
import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.advisor.MessageChatMemoryAdvisor;
import org.springframework.ai.chat.client.advisor.QuestionAnswerAdvisor;
import org.springframework.ai.chat.memory.InMemoryChatMemory;
import org.springframework.ai.chat.metadata.ChatResponseMetadata;
import org.springframework.ai.chat.model.ChatResponse;
import org.springframework.ai.document.Document;
import org.springframework.ai.vectorstore.SearchRequest;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.List;

import static org.springframework.ai.chat.client.advisor.AbstractChatMemoryAdvisor.CHAT_MEMORY_CONVERSATION_ID_KEY;

@Service
public class AiService {

    private final ChatClient aiClient;
    private final MetadataService metadataService;
    private final VectorStore vectorStore;

    @Value("classpath:/promptTemplates/musicAiSystemPrompt.st")
    Resource musicAiSystemPrompt;

    @Value("classpath:/promptTemplates/userTextAdvice.st")
    Resource userTextAdvise;

    public AiService(ChatClient.Builder aiClient, MetadataService metadataService, VectorStore vectorStore) {
        this.aiClient = aiClient
                .defaultAdvisors(new MessageChatMemoryAdvisor(new InMemoryChatMemory()))
                .build();
        this.metadataService = metadataService;
        this.vectorStore = vectorStore;
    }

    public AuthorsDto getInfoAboutAuthorOldWithMetadata(String author) {
        var promptString = """
                Розкажи мені більше за автора {author}.
                """;
        ChatResponse chatResponse = aiClient.prompt()
                .system(systemSpec -> systemSpec.text(musicAiSystemPrompt))
                .user(u -> u.text(promptString)
                        .param("author", author))
                .call()
                .chatResponse();

        AuthorsDto authorsDto = new AuthorsDto(author, chatResponse.getResult().getOutput().getContent());

        ChatResponseMetadata metadata = chatResponse.getMetadata();
        metadataService.registerUsage(metadata.getUsage());

        return authorsDto;
    }

    public String getInfoAboutAuthor(String author) {
        var promptString = """
                Tell me more about the music artist %s.
                """.formatted(author);

        SearchRequest searchRequest = SearchRequest.query(promptString).withTopK(2);

        List<Document> documents = vectorStore.similaritySearch(searchRequest);

        return aiClient.prompt()
                .user(promptString)
                .advisors(new QuestionAnswerAdvisor(vectorStore, searchRequest, readResource(userTextAdvise)))
                .call()
                .content();
    }

    @SneakyThrows
    private String readResource(Resource resource) {
        return resource.getContentAsString(Charset.defaultCharset());
    }

    public String ask(QuestionDto questionDto, String conversationId) {
        String question = questionDto.question();

        SearchRequest searchRequest = SearchRequest.query(question).withTopK(2);

        List<Document> documents = vectorStore.similaritySearch(searchRequest);

        return aiClient.prompt()
                .user(question)
                .advisors(new QuestionAnswerAdvisor(vectorStore, searchRequest, readResource(userTextAdvise)))
                .advisors(advisorSpec ->
                        advisorSpec.param(CHAT_MEMORY_CONVERSATION_ID_KEY, conversationId))
                .call()
                .content();
    }
}
