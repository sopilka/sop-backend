package com.sopilka.backend.domain;


import com.sopilka.backend.domain.author.Author;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.Instant;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String title;
    private int price;
    private int quantity;
    private String label;
    private int releaseYear;
    @Enumerated(EnumType.STRING)
    private ItemType itemType;
    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ImageUrl> imageUrls = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "author_items",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Set<Author> authors = new HashSet<>();

    @ElementCollection
    @CollectionTable(
            name = "item_genres",
            joinColumns = @JoinColumn(name = "item_id")
    )
    @Column(name = "genre")
    private Set<String> genres = new HashSet<>();

    @CreatedDate
    Instant createdDate;
    @LastModifiedDate
    Instant lastModifiedDate;
    @Version
    private Integer version;

    public Item(String title, int price, int quantity, String label, int releaseYear, ItemType itemType) {
        this.title = title;
        this.price = price;
        this.quantity = quantity;
        this.label = label;
        this.releaseYear = releaseYear;
        this.itemType = itemType;
    }
}
