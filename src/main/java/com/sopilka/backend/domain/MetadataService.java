package com.sopilka.backend.domain;

import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ai.chat.metadata.Usage;
import org.springframework.stereotype.Service;

@Service
public class MetadataService {

    private static final Logger log =
            LoggerFactory.getLogger(MetadataService.class);

    private final MeterRegistry meterRegistry;

    public MetadataService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void registerUsage(Usage usage) {
        long promptTokenCount = usage.getPromptTokens();
        long genTokenCount = usage.getGenerationTokens();
        long totalTokenCount = usage.getTotalTokens();

        log.info("Token usage: prompt={}, generation={}, total={}",
                promptTokenCount, genTokenCount, totalTokenCount);

        meterRegistry
                .counter("ai.usage.tokens", "token-type", "prompt")
                .increment(promptTokenCount);
        meterRegistry
                .counter("ai.usage.tokens", "token-type", "generation")
                .increment(genTokenCount);
    }
}