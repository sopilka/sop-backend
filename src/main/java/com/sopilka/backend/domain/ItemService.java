package com.sopilka.backend.domain;

import com.sopilka.backend.domain.author.Author;
import com.sopilka.backend.domain.author.AuthorRepository;
import com.sopilka.backend.domain.genre.GenreService;
import com.sopilka.backend.web.AddItem;
import com.sopilka.backend.web.ItemDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class ItemService {

    final S3Service s3Service;
    final GenreService genreService;
    final ItemRepository itemRepository;
    final AuthorRepository authorRepository;
    final ImageUrlRepository imageUrlRepository;

    public ItemService(S3Service s3Service, GenreService genreService, ItemRepository itemRepository, AuthorRepository authorRepository, ImageUrlRepository imageUrlRepository) {
        this.s3Service = s3Service;
        this.genreService = genreService;
        this.itemRepository = itemRepository;
        this.authorRepository = authorRepository;
        this.imageUrlRepository = imageUrlRepository;
    }

    public List<ItemDto> findAll() {
        return itemRepository.findAll().stream().map(el ->
                ItemDto.builder()
                        .title(el.getTitle())
                        .price(el.getPrice())
                        .itemType(el.getItemType())
                        .releaseYear(el.getReleaseYear())
                        .label(el.getLabel())
                        .imageUrls(el.getImageUrls().stream().map(ImageUrl::getUrl).toList())
                        .authors(el.getAuthors().stream().map(Author::getName).toList())
                        .genres(el.getGenres())
                        .build()).toList();
    }

    public Item findById(UUID id) {
        return itemRepository.findById(id)
                .orElseThrow(ItemNotFoundException::new);
    }

    public Item add(AddItem itemToAdd) {
        Item savedItem = itemRepository.save(buildItem(itemToAdd));
        addImages(itemToAdd, savedItem);
        return savedItem;
    }

    public void delete(UUID id) {
        itemRepository.deleteById(id);
    }

    public void addGenre(UUID itemId, String genre) {
        if (!genreService.exists(genre)) {
            return;
        }

        Item item = findById(itemId);
        Set<String> genres = item.getGenres();
        genres.add(genre);
        itemRepository.save(item);
    }

    private void addImages(AddItem itemToAdd, Item savedItem) {
        for (MultipartFile image : itemToAdd.images()) {
            String url = s3Service.uploadFile(image);
            imageUrlRepository.save(new ImageUrl(savedItem, url));
        }
    }

    private Item buildItem(AddItem itemToAdd) {
        var itemToSave = new Item(itemToAdd.title(), 0, 1,
                itemToAdd.label(), itemToAdd.releaseYear(), ItemType.valueOf(itemToAdd.itemType()));

        List<Author> authors = authorRepository.findAllById(List.of(itemToAdd.authorIds()));
        itemToSave.setAuthors(new HashSet<>(authors));
        return itemToSave;
    }
}
