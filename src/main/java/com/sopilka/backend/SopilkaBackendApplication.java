package com.sopilka.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SopilkaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SopilkaBackendApplication.class, args);
	}

}
