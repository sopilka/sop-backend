CREATE TABLE image_urls
(
    id         SERIAL PRIMARY KEY,
    item_id    UUID REFERENCES item (id),
    url        TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);