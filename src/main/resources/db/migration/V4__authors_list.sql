CREATE TABLE author
(
    id                 UUID PRIMARY KEY NOT NULL,
    name               TEXT,
    created_date       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_modified_date timestamp        NOT NULL,
    version            integer          NOT NULL
);

CREATE TABLE author_items
(
    item_id   UUID REFERENCES item (id),
    author_id UUID REFERENCES author (id)
);

ALTER TABLE item DROP COLUMN author;