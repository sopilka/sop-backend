CREATE TABLE item
(
    id                 UUID PRIMARY KEY NOT NULL,
    title              varchar(255)     NOT NULL,
    price              integer          NOT NULL,
    quantity           integer          NOT NULL,
    author             varchar(255)     NOT NULL,
    label              varchar(255),
    release_year       integer          NOT NULL,
    item_type          varchar(15)      NOT NULL,
    created_date       timestamp        NOT NULL,
    last_modified_date timestamp        NOT NULL,
    version            integer          NOT NULL
);