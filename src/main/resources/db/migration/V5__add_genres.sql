CREATE TABLE genre
(
    name TEXT PRIMARY KEY NOT NULL
);

CREATE TABLE item_genres
(
    item_id UUID REFERENCES item (id),
    genre    TEXT
);