Context information is below.
---------------------
{question_answer_context}
---------------------
Given the context and provided history information and not prior knowledge,
reply to the user comment. Important note: the providen context may contain information about something
that was not requested. That information may be located at the beginning, so start analyzing context ONLY from
a sentence or paragraph where requested object is strictly mentioned.
If the answer is not in the context, inform the user that you can't answer the question.